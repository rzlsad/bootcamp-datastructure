package az.ingress.collection.dto;

import lombok.Data;
import lombok.Getter;

import java.util.Comparator;
import java.util.Objects;

@Data
public class Node<T extends Comparable> implements Comparable<Node<T>>{
    private Node<T> prev;
    private Node<T> left;
    private Node<T> right;
    private final T value;
    private boolean leftNode;
    public Node(T value) {
        this.value = value;
        left = null;
        right = null;
        prev = null;
        leftNode = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> node = (Node<?>) o;
        return Objects.equals(value, node.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public int compareTo(Node<T> tNode) {
        if (Objects.isNull(tNode)) {
            throw new IllegalArgumentException("invalid argument passed");
        }
        return value.compareTo(tNode.getValue());
    }


}
