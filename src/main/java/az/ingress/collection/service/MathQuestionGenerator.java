package az.ingress.collection.service;

import az.ingress.collection.service.impl.MathQuestionGeneratorImp;

import java.io.File;

public interface MathQuestionGenerator {
    String generate();

    static MathQuestionGenerator getQuestionGenerator() {
        return new MathQuestionGeneratorImp();
    }
}
