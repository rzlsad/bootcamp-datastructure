package az.ingress.collection.service;

public interface HashTable<T> extends Iterable<T>{

    boolean add(T item);
    void remove(T item);
    boolean contains(T item);
}
