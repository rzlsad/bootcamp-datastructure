package az.ingress.collection.service;

import az.ingress.collection.service.impl.CalculatorImpl;

public interface Calculator {
    double solve();

    static Calculator getCalculator(String expression) {
        return new CalculatorImpl(expression);
    }
}
