package az.ingress.collection.service.impl;


import az.ingress.collection.service.Calculator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
@Setter
@Getter
public class CalculatorImpl implements Calculator {

    private String expression;

    @Override
    public double solve() {
        calculateFactorial();
        return new ExpressionBuilder(expression)
                .build()
                .evaluate();
    }

    private int fact(int i) {
        if (i <= 1) {
            return 1;
        }else {
            return i*fact(i-1);
        }
    }

    private String calculateFactorial() {
        Matcher matcher = Pattern.compile("(\\d*\\d*\\d)+!").matcher(expression);
        if (matcher.find()){
            int fact = fact(Integer.valueOf(matcher.group(1)));
            expression = matcher.replaceFirst(String.valueOf(fact));
            return calculateFactorial();
        } else {
            return expression;
        }
    }
}
