package az.ingress.collection.service.impl;

import az.ingress.collection.service.Calculator;
import az.ingress.collection.service.MathProblemSolver;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.Deque;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
@Getter
public class MathProblemSolverWithRawThreadsImpl implements MathProblemSolver {
    private static final String END = "END";
    private static final int FETCHER_AND_MAIN_THREADS = 2;
    private int cores;
    private BlockingQueue<String> expressionsQueue;
    private Deque<Double> results;
    private final File file;
    private DoubleSummaryStatistics summaryStatistics;


    private final Runnable solve;
    private final Runnable fetch;

    public MathProblemSolverWithRawThreadsImpl(File file) {
        cores = Runtime.getRuntime().availableProcessors() - FETCHER_AND_MAIN_THREADS;
        expressionsQueue = new LinkedBlockingQueue<>();
        results = new ConcurrentLinkedDeque<>();
        this.file = file;
        solve = () -> {
            while (true) {
                String expression = expressionsQueue.poll();
                if (END.equalsIgnoreCase(expression)) {
                    expressionsQueue.offer(expression);
                    return;
                }
                if (Objects.nonNull(expression)
                        && !expression.trim().isEmpty()) {
                    results.add(Calculator.getCalculator(expression).solve());
                }
            }
        };
        fetch = () -> {
            try (Stream<String> lines = Files.lines(file.toPath())) {
                lines.forEach(expressionsQueue::offer);
                expressionsQueue.offer(END);
            } catch (IOException ioe) {
                throw new RuntimeException("File IO exception: "+ioe.getMessage(),ioe);
            }
        };
    }

    private synchronized void solve(){

        Thread fetcher = new Thread(fetch);
        fetcher.start();

        List<Thread> solvers = Stream.generate(this::getSolve)
                .limit(cores)
                .map(Thread::new)
                .peek(Thread::start)
                .collect(Collectors.toList());

        solvers.forEach(thread -> {
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                });

        summaryStatistics = results.stream()
                .mapToDouble(Double::doubleValue)
                .summaryStatistics();
    }

    public BigDecimal getMax() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getMax());
    }

    public BigDecimal getMin() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getMin());
    }

    public BigDecimal getSum() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getSum());
    }

    private void checkIfAlreadySolved() {
        if (summaryStatistics == null) {
            synchronized (this) {
                if (summaryStatistics == null) {
                    solve();
                }
            }
        }
    }
    public static void main(String[] args) {
        File file = new File("./question.txt");
        if (file.exists()) {
            StopWatch stopWatch = new StopWatch();
            MathProblemSolver mathProblemSolver = MathProblemSolver.getMathProblemSolverWithRawThreads(file);
            stopWatch.start();
            BigDecimal min = mathProblemSolver.getMin();
            BigDecimal max = mathProblemSolver.getMax();
            BigDecimal sum = mathProblemSolver.getSum();
            stopWatch.stop();
            log.info("time spend: {}", stopWatch.getTotalTimeMillis());
            log.info("min: {}",min);
            log.info("max: {}",max);
            log.info("sum: {}",sum);
        } else {
            System.out.println("no such file");
        }
    }


}
