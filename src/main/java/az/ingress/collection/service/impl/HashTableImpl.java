package az.ingress.collection.service.impl;

import az.ingress.collection.service.HashTable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class HashTableImpl<T> implements HashTable<T> {

    private static final int DEFAULT_BUCKETS_SIZE = 16;
    private static final int DEFAULT_LOAD_PERCENT = 75;

    private List<? extends Collection<T>> buckets;
    private final int loadPercent;
    private int numberOfItems;
    private int size;

    public HashTableImpl() {
        this.size = DEFAULT_BUCKETS_SIZE;
        this.loadPercent = DEFAULT_LOAD_PERCENT;
        numberOfItems = 0;
        buckets = Stream.generate(LinkedList<T>::new)
                .limit(size)
                .collect(Collectors.toList());
    }

    @Override
    public boolean add(T item) {
        if (Objects.isNull(item)) {
            throw new IllegalArgumentException("invalid argument inserted");
        }
        int index = hashToIndex(item);
        boolean willBeDistinct = checkIfNotExists(index, item);
        if (willBeDistinct) {
            buckets.get(index).add(item);
            numberOfItems++;
            checkBucketSize();
        }
        return willBeDistinct;
    }

    @Override
    public void remove(T item) {
        if (Objects.isNull(item)) {
            throw new IllegalArgumentException("invalid argument inserted");
        }
        int index = hashToIndex(item);
        boolean contains = buckets.get(index).contains(item);
        if (contains) {
            buckets.get(index).remove(item);
            numberOfItems--;
            if (numberOfItems < 0) {
                throw new IllegalStateException("number of items cannot be less than zero");
            }
            checkBucketSize();
        }
    }

    @Override
    public boolean contains(T item) {
        int index = hashToIndex(item);
        return buckets.get(index).contains(item);
    }

    @Override
    public Iterator iterator() {
        return new Iterator();
    }

    @Override
    public String toString() {
        return "HashTableImpl{" +
                "buckets=" + buckets +
                '}';
    }

    public class Iterator implements java.util.Iterator<T> {

        private int index;
        private java.util.Iterator<T> iterator;

        public Iterator() {
            index = 0;
            if (index < size) {
                iterator = buckets.get(index).iterator();
            } else {
                iterator = null;
            }
        }

        @Override
        public boolean hasNext() {

            while (!iterator().hasNext()) {
                if (index < (size - 1)) {
                    index++;
                    iterator = buckets.get(index).iterator();
                } else {
                    break;
                }
            }
            if (iterator.hasNext()) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public T next() {
            if (Objects.isNull(iterator)) {
                throw new IllegalStateException("iterator must be checked before calling next");
            }
            return iterator.next();
        }
    }

    private void checkBucketSize() {
        int loaded = numberOfItems*100/size;
        if (loaded > loadPercent) {
            size *= 2;
            List<? extends Collection<T>> oldBucket = buckets;
             buckets = Stream.generate(LinkedList<T>::new)
                    .limit(size)
                    .collect(Collectors.toList());
            oldBucket.stream()
                    .flatMap(Collection::stream)
                    .forEach(this::add);
        } else if (4 * loaded < loadPercent) {
            size /= 2;
            List<? extends Collection<T>> oldBucket = buckets;
            buckets = Stream.generate(LinkedList<T>::new)
                    .limit(size)
                    .collect(Collectors.toList());
            oldBucket.stream()
                    .flatMap(Collection::stream)
                    .forEach(this::add);
        }
    }

    private int hashToIndex(T item) {
        int hashCode = item.hashCode();
        return hashCode % size;
    }

    private boolean checkIfNotExists(int index, T item) {
        Collection<T> bucket = buckets.get(index);
        return !bucket.contains(item);
    }

}
