package az.ingress.collection.service.impl;

import az.ingress.collection.service.MathQuestionGenerator;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.stream.Stream;

public class MathQuestionGeneratorImp implements MathQuestionGenerator {

    private static final int NUMBER_UPPER_BOUND = 1000;
    private static final int OPERATION_UPPER_BOUND = 4;
    private final Random randomInt;
    private final Random randomOperation;

    public MathQuestionGeneratorImp() {
        randomInt = new Random(0){
            @Override
            public int nextInt() {
                int i;
                do{
                   i = super.nextInt(NUMBER_UPPER_BOUND);
                }while (i<=0);
                return i;
            }
        };
        randomOperation = new Random(0){
            public int nextInt() {
                return super.nextInt(OPERATION_UPPER_BOUND);
            }
        };
    }

    @Override
    public String generate() {
        StringBuffer sb = new StringBuffer(0);
        sb.append(randomInt.nextInt())
                .append(convertToOperation(randomOperation.nextInt()))
                .append(convertToFactorial(randomInt.nextInt()))
                .append(convertToOperation(randomOperation.nextInt()))
                .append(convertToFactorial(randomInt.nextInt()))
                .append(convertToOperation(randomOperation.nextInt()))
                .append(convertToFactorial(randomInt.nextInt()))
                .append(convertToOperation(randomOperation.nextInt()))
                .append(convertToFactorial(randomInt.nextInt()))
                .append(convertToOperation(randomOperation.nextInt()))
                .append(convertToFactorial(randomInt.nextInt()))
                .append(System.lineSeparator());
        return sb.toString();
    }

    private String convertToOperation(final int operation) {
        switch (operation) {
            case 0 : return "+";
            case 1 : return "-";
            case 2 : return "*";
            case 3 : return "/";
            default: return "";
        }
    }



    private String convertToFactorial(int number) {
        if (randomInt.nextBoolean()
        && number < 7) {
            return String.valueOf(number).concat("!");
        }else {
            return String.valueOf(number);
        }
    }

    public static void main(String[] args) throws IOException {
        OutputStream outputStream = Files.newOutputStream(Path.of(".", "question.txt"));
        PrintStream printStream = new PrintStream(outputStream);
        MathQuestionGenerator questionGenerator = MathQuestionGenerator.getQuestionGenerator();
        Stream.generate(questionGenerator::generate).limit(1000_000L).forEach(printStream::print);
    }

}
