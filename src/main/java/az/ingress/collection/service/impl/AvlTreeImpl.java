package az.ingress.collection.service.impl;

import az.ingress.collection.dto.Node;
import az.ingress.collection.service.AvlTree;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@Getter
public final class AvlTreeImpl<T extends Comparable> implements AvlTree<T> {

    private Node<T> root;
    private int size;

    @Override
    public void add(T value) {
        if (Objects.isNull(root)) {
            root = new Node<>(value);
            size = 1;
        } else {
            add(root, new Node<>(value));
            size++;
        }
    }

    @Override
    public Iterator iterator() {
        return new Iterator();
    }
    public class Iterator implements java.util.Iterator<T> {

        private Node<T> pointer;
        private boolean left;
        private int level;

        public Iterator() {
            pointer = root;
            level = 0;
            left = true;
        }

        @Override
        public boolean hasNext() {
            if (left
            && Objects.nonNull(pointer.getLeft())) {
                pointer = getSmallestNode(pointer);
            } else if (left) {
                pointer = pointer.getPrev();
                left = false;
            } else {
                if (Objects.nonNull(pointer.getRight())
                && Objects.nonNull(pointer.getRight().getLeft())) {
                    pointer = getSmallestNode(pointer.getRight());
                    left = true;
                } else if (Objects.nonNull(pointer.getRight())) {
                    pointer = pointer.getRight();
                } else {
                    pointer = getLeftParent(pointer.getPrev());
                    left = false;
                }
            }
            return pointer != null;
        }

        @Override
        public T next() {
            return pointer.getValue();
        }

        private Node<T> getSmallestNode(Node<T> node) {
            if (Objects.isNull(node)) {
                return null;
            } else if (Objects.isNull(node.getLeft())) {
                level++;
                return node;
            } else {
                level++;
                return getSmallestNode(node.getLeft());
            }
        }

        private Node<T> getLeftParent(Node<T> parent) {
            if (Objects.isNull(parent)) {
                return null;
            } else if (parent.isLeftNode()) {
                return parent.getPrev();
            } else {
                return getLeftParent(parent.getPrev());
            }
        }
    }
    private void add(Node<T> parent, Node<T> child) {
        boolean left = parent.compareTo(child) > 0;

        if (left) {
            if (Objects.isNull(parent.getLeft())) {
                parent.setLeft(child);
                child.setPrev(parent);
                child.setLeftNode(true);
            } else {
                add(parent.getLeft(), child);
            }
        } else {
            if (Objects.isNull(parent.getRight())) {
                parent.setRight(child);
                child.setPrev(parent);
                child.setLeftNode(false);
            } else {
                add(parent.getRight(), child);
            }
        }
    }

}
