package az.ingress.collection.service.impl;

import az.ingress.collection.service.Calculator;
import az.ingress.collection.service.MathProblemSolver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.DoubleSummaryStatistics;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class MathProblemSolverParallelStreamImpl implements MathProblemSolver {
    private DoubleSummaryStatistics summaryStatistics;
    private final File file;

    private void solve() {
        try (Stream<String> lines = Files.lines(file.toPath())) {
            summaryStatistics = lines
                    .parallel()
                    .map(Calculator::getCalculator)
                    .mapToDouble(Calculator::solve)
                    .summaryStatistics();
        } catch (IOException ioe) {
            throw new RuntimeException("File IO exception: "+ioe.getMessage(),ioe);
        }

    }

    public BigDecimal getMax() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getMax());
    }

    public BigDecimal getMin() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getMin());
    }

    public BigDecimal getSum() {
        checkIfAlreadySolved();
        return BigDecimal.valueOf(summaryStatistics.getSum());
    }

    private void checkIfAlreadySolved() {
        if (summaryStatistics == null) {
            synchronized (this) {
                if (summaryStatistics == null) {
                    solve();
                }
            }
        }
    }

    public static void main(String[] args) {
        File file = new File("./question.txt");
        if (file.exists()) {
            StopWatch stopWatch = new StopWatch();
            MathProblemSolver mathProblemSolver = MathProblemSolver.getMathProblemSolverWithParalelStream(file);
            stopWatch.start();
            BigDecimal min = mathProblemSolver.getMin();
            BigDecimal max = mathProblemSolver.getMax();
            BigDecimal sum = mathProblemSolver.getSum();
            stopWatch.stop();
            log.info("time spend: {}", stopWatch.getTotalTimeMillis());
            log.info("min: {}",min);
            log.info("max: {}",max);
            log.info("sum: {}",sum);
        } else {
            System.out.println("no such file");
        }
    }

}
