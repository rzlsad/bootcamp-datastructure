package az.ingress.collection.service;

import az.ingress.collection.service.impl.MathProblemSolverParallelStreamImpl;
import az.ingress.collection.service.impl.MathProblemSolverSequentialStreamImpl;
import az.ingress.collection.service.impl.MathProblemSolverWithRawThreadsImpl;

import java.io.File;
import java.math.BigDecimal;

public interface MathProblemSolver {

    BigDecimal getMin();
    BigDecimal getMax();
    BigDecimal getSum();

    static MathProblemSolver getMathProblemSolverWithParalelStream(File file) {

        return new MathProblemSolverParallelStreamImpl(file);
    }

    static MathProblemSolver getMathProblemSolverWithRawThreads(File file) {
        return new MathProblemSolverWithRawThreadsImpl(file);
    }

    static MathProblemSolver getMathProblemSolverWithSequentialStream(File file) {
        return new MathProblemSolverSequentialStreamImpl(file);
    }
}
