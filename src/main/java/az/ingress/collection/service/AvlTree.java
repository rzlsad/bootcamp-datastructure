package az.ingress.collection.service;

public interface AvlTree<T extends Comparable> extends Iterable<T>{

    void add(T value);

}
