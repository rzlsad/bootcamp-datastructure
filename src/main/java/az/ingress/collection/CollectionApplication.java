package az.ingress.collection;

import az.ingress.collection.service.HashTable;
import az.ingress.collection.service.MathProblemSolver;
import az.ingress.collection.service.impl.AvlTreeImpl;
import az.ingress.collection.service.impl.HashTableImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StopWatch;

import java.io.File;
import java.math.BigDecimal;

@SpringBootApplication
@Slf4j
public class CollectionApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CollectionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		File file = new File("./question.txt");
		if (file.exists()) {
			//Parallel Streaming
			StopWatch stopWatch = new StopWatch();
			MathProblemSolver mathProblemSolver = MathProblemSolver.getMathProblemSolverWithParalelStream(file);
			stopWatch.start();
			BigDecimal min = mathProblemSolver.getMin();
			BigDecimal max = mathProblemSolver.getMax();
			BigDecimal sum = mathProblemSolver.getSum();
			stopWatch.stop();
			log.info("Parallel Streaming");
			log.info("time spend: {}", stopWatch.getTotalTimeMillis());
			log.info("min: {}",min);
			log.info("max: {}",max);
			log.info("sum: {}",sum);
			//Multiple Threads
			stopWatch = new StopWatch();
			mathProblemSolver = MathProblemSolver.getMathProblemSolverWithRawThreads(file);
			stopWatch.start();
			min = mathProblemSolver.getMin();
			max = mathProblemSolver.getMax();
			sum = mathProblemSolver.getSum();
			stopWatch.stop();
			log.info("Multiple Threads");
			log.info("time spend: {}", stopWatch.getTotalTimeMillis());
			log.info("min: {}",min);
			log.info("max: {}",max);
			log.info("sum: {}",sum);
			//Single Thread
			stopWatch = new StopWatch();
			mathProblemSolver = MathProblemSolver.getMathProblemSolverWithSequentialStream(file);
			stopWatch.start();
			min = mathProblemSolver.getMin();
			max = mathProblemSolver.getMax();
			sum = mathProblemSolver.getSum();
			stopWatch.stop();
			log.info("Single Thread");
			log.info("time spend: {}", stopWatch.getTotalTimeMillis());
			log.info("min: {}",min);
			log.info("max: {}",max);
			log.info("sum: {}",sum);
		} else {
			System.out.println("no such file");
		}
	}
}
